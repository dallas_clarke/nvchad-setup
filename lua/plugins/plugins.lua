local plugins = {
  { "lewis6991/gitsigns.nvim",

    opts = {
      current_line_blame = true,
    },
  },

  {

  "kylechui/nvim-surround",
   version = "*", -- Use for stability; omit to use `main` branch for the latest features
    event = "VeryLazy",
    config = function()
        require("nvim-surround").setup({
            -- Configuration here, or leave empty to use defaults
          normal = "ys",
          normal_cur = "yss",
          normal_line = "yS",
          normal_cur_line = "ySS",
          visual = "S",
          visual_line = "gS",
          delete = "ds",
          change = "cs",
        })
    end

  },
}

return plugins
