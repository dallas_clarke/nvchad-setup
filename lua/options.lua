require "nvchad.options"

local opt = vim.opt

opt.relativenumber = true

-- Enabling OSC52
vim.opt.clipboard = "unnamedplus"

local function has_osc52()
  -- Check if we're in a SSH session or a terminal that likely supports OSC52
  return vim.env.SSH_TTY ~= nil or vim.env.TERM_PROGRAM == "iTerm.app"
end

if has_osc52() then
  -- Use OSC52 when in a remote session
  vim.g.clipboard = {
    name = 'osc52',
    copy = {
      ['+'] = require('vim.clipboard.osc52').copy,
      ['*'] = require('vim.clipboard.osc52').copy,
    },
    paste = {
      ['+'] = require('vim.clipboard.osc52').paste,
      ['*'] = require('vim.clipboard.osc52').paste,
    },
  }
elseif vim.fn.executable('xclip') == 1 then
  -- Use xclip for local sessions on X11
  vim.g.clipboard = {
    name = 'xclip',
    copy = {
      ['+'] = 'xclip -selection clipboard',
      ['*'] = 'xclip -selection clipboard',
    },
    paste = {
      ['+'] = 'xclip -selection clipboard -o',
      ['*'] = 'xclip -selection clipboard -o',
    },
    cache_enabled = 1,
  }
end

